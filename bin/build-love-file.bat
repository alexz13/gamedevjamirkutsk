set dt=%date%-%time%
set myTime=%dt::=.%
set myTimeEx=%myTime: =0%
set newdirname=%myTimeEx:~0,-6%
mkdir .\build\%newdirname%

7za a -tzip build.love ../project/*
copy build.love .\build\%newdirname%\game.love
rm build.love
cd ..
pause