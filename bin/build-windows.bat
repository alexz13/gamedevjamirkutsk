set dt=%date%-%time%
set myTime=%dt::=.%
set myTimeEx=%myTime: =0%
set newdirname=%myTimeEx:~0,-6%
mkdir .\build\%newdirname%

7za a -tzip ./love/build.love ../project/*
cd love
copy /b love.exe+build.love ..\build\%newdirname%\game.exe
copy *.dll ..\build\%newdirname%\
copy license.txt ..\build\%newdirname%\license.txt
rm build.love
cd ..
pause