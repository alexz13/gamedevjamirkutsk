# Credits #

Document contains resources used in game. We use it to reconcile all the licenses. Also we have to mention authors in credits.

##  Images  ##
- [Tileset form OpenGameArt.org by Buch](http://opengameart.org/content/nihil-ace-spaceship-building-pack-expansion)

    License: [CC-BY 3.0](http://creativecommons.org/licenses/by/3.0/) 
    Instructions: Credit author as Buch and link back to his OGA profile page.
- [SpaceBackgrounds form OpenGameArt.org by Rawdanitsu](http://opengameart.org/content/space-backgrounds-7)
    License: [CC-0](http://creativecommons.org/publicdomain/zero/1.0/) 
    Instructions: All my work on OpenGameArt is 100% free and PUBLIC DOMAIN. Use it as you like (commercialy or non commercialy - I DON'T CARE :) and no atribution needed.

## Music ##

