useDebugConsole = false
if useDebugConsole then
  require("libs.lovedebug")
end

-- утилиты для удобства разработки и отладки
utils = require("src.utils")
-- процедуры инициализации и загрузки медиа ресурсов (img,snd,fnt,vid)
resources = require("src.resources")
-- интерфейсы игры
interface = require("src.interface")
-- игровые состояния
states = require("src.states")
-- элементы игровой механики
mech  = require("src.mech")
-- физика
phy = require('src.physics')

function love.load(arg)
  if not(useDebugConsole)then
    -- add this for debugging in zbstudio
    if arg[#arg] == "-debug" then require("mobdebug").start(); end
  end
	love.window.setMode( 800,800, {resizable=false, vsync=false} )
  
  resources:load()
  interface:init()
  phy:init()
  states:init()
-- states:switch("phyTest")
  states:switch("test")
end

function love.quit()

end
