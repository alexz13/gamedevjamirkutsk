
local gamesaver = {}
gamesaver.serializer = require('libs.Ser.ser');


function gamesaver:init()
  self.savedir = 'skite'
  e = love.filesystem.exists( self.savedir )
  if not e then
    love.filesystem.createDirectory( self.savedir )
  end
end


function gamesaver:saveMap(maptable)
  if(maptable ~= nil) then
    s = gamesaver.serializer(maptable)
    love.filesystem.write( self.savedir..'/map.save', s )
  end
end

function gamesaver:loadMap()
  --grid.map, _ = love.filesystem.read( self.savedir..'/map.save')
  e = love.filesystem.exists( self.savedir..'/map.save' )
  if e then
    s, _ = love.filesystem.read( self.savedir..'/map.save')
    grid.map = loadstring(s)()

    --grid:copyForm(map)
  end
  
end

return(gamesaver)