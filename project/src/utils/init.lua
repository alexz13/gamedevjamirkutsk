local BASE = (...) .. "."

local printer = require(BASE .. "print")


local utils = {
	tostring = printer.tostring,
	print = printer.print,
  --recorder = require("libs.GifCat")  
}

--function utils:recorderInit(periodms)
--      self.recordPeriod = periodms
--      self.recordPeriodTimer = 0
--      self.gifcat.init()
--      filename = os.date()
--      filename = string.gsub(filename, "/", "-")
--      filename = string.gsub(filename, " ", "_")
--      filename = string.gsub(filename, ":", "-")
--      self.record =  gifcat.newGif(filename..".gif",love.window.getWidth(),love.window.getHeight())
--end
  
--function utils:recorderUpdate(dt)
--    self.recordPeriodTimer = self.recordPeriodTimer + dt
--    if( self.recordPeriodTimer > self.recordPeriod) then
--      self.recordPeriodTimer = 0
--      self.record:frame(love.graphics.newScreenshot())
--    end
--end
  
--function utils:recorderStop()
--    self.record:close()
--end



function utils.newArea(left,top,width,height)
  local area = {
      left = 0, 
      top = 0,
      right = left + width,
      bottom = top + height
    }
    
    function area:isInside(x,y)
        return( x >= self.left and
                y >= self.top and
                x <= self.right and
                y <= self.bottom 
              )
    end
    function area:height()
        return( self.bottom - self.top  )
    end
    
    function area:width()
        return( self.right - self.left )
    end
    
  return area
end

return utils