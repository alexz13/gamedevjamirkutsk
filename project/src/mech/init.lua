    local mech = {
          -- типы блоков
      BLOCK_EMPTY = 0,
      BLOCK_POWER = 1,
      BLOCK_WIRE = 2,
      BLOCK_MOVEUP = 3,
      BLOCK_MOVEDOWN = 4,
      BLOCK_MOVELEFT = 5,
      BLOCK_MOVERIGHT= 6,

      BLOCK_SEPARATOR = 7,
      BLOCK_LOCATOR = 8,
      BLOCK_CONTAINER = 9,

      BLOCK_ASTEROID = 10,
      
      BLOCK_POWERED_MASK = 100,
      
      getBlockType = function(name)
          return(self[name])
      end
      
    }
    
    
    return mech