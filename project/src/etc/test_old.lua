local intro = {}

function intro:init()
  intro.updateTime = 50
  intro.timeToUpdate = intro.updateTime
  width, height = resources.img.bgimage:getDimensions( )
  cam = interface.camera.new(width/2,height/2,1,0)
  
  interface.interfacePanel.visible = true;
  
  interface.grid.visible = true;
end

function intro:update(dt)
  
  local dx = 0
  local dy = 0
  local cameraStep = 0.25
  
  local xpos, ypos = love.mouse.getPosition()
  
  if xpos <= interface.cameraMovingRanges.left then
    dx = -cameraStep
  elseif xpos >= interface.cameraMovingRanges.right then
    dx = cameraStep
  end
  if ypos <= interface.cameraMovingRanges.top then
    dy = -cameraStep
  elseif ypos >= interface.cameraMovingRanges.bottom and ypos < (interface.cameraMovingRanges.bottom + interface.cameraMovingBound) then
    dy = cameraStep
  end 
    
  if dx ~= 0 or dy ~= 0  then
      local width, height = love.graphics.getDimensions()
      local l,t = cam:worldCoords(0,0)
      local r,b = cam:worldCoords(width,height)
      
    if l <= interface.cameraRanges.left and dx < 0 then dx = 0; end
    if t <= interface.cameraRanges.top and dy < 0 then dy = 0; end
    if r >= interface.cameraRanges.right and dx > 0 then dx = 0; end
    if b >= interface.cameraRanges.bottom and dy > 0 then dy = 0; end
    
    cam:move(dx,dy)
    
  end

end

function intro:draw()
    cam:attach()
        love.graphics.setColor( 255, 255, 255 )
        love.graphics.draw(resources.img.bgimage, 0, 0)
    cam:detach()
                                                                  
    interface:draw()
                                
end

function intro:exitToMenu()
--  collectgarbage()
--  states.switch(menu)
end

function intro:keypressed( key, scancode, isrepeat )
--  intro:exitToMenu()
end

function intro:wheelmoved(x, y)
  interface.screenCamera:wheelmoved(x, y)
--    if y > 0 then
--        cam:zoom(1.25)
--    elseif y < 0 then
--        cam:zoom(0.8)
--    end
end
  
function intro:mousepressed(x,y,b)

end
  
return(intro)