 local constructor = {}
 
function constructor:init()
  self.simulation = false;
  self.simulationTime = 0;
  self.simulationStep = 1000;
  grid:init()
  interfacePanel:init( {1, 2, 3} )
  textpanel:init()
end

function constructor:enter(previous) -- runs every time the state is entered
    
end

function constructor:leave()
  --  snd:stop()
end

function constructor:keypressed( key, scancode, isrepeat )
  if scancode=='space' then
    grid:activation()
    grid:action()
  elseif scancode=='0' then
    grid.currentBlockType = 0
  elseif scancode=='w' then
    grid.workArea.enable = not grid.workArea.enable
  elseif scancode=='a' then
    grid:activation()
  elseif scancode=='x' then
    grid.showcells = not grid.showcells
  elseif scancode=='`' then
    grid:deActivateAll()
  elseif scancode=='escape' then
    Gamestate.push(menu)
  end
end

function constructor:update(dt)
  
  if self.simulation==true then
    self.simulationTime = self.simulationTime + dt;
    self.simulationStep = 1000;
  end
  
   local is_down_m = love.mouse.isDown(1)
   if is_down_m then
    local cell = grid:pickCell()
    if cell ~= nil then
      grid.map[cell.x][cell.y] = grid.makeBlock(grid.currentBlockType)
    end
   end

  interfacePanel:checkIfBlocksPicked(love.mouse.getX(), love.mouse.getY());
  textpanel:update()
end

function constructor:draw()
  love.graphics.setColor( 255, 255, 255 )
	love.graphics.draw(gfx.bgimage, 0, 0)
	grid:draw()
	grid:drawGridCells()
	interfacePanel:draw()
  textpanel:draw()
  suit.draw()
end

return(constructor)