local grid = {}

function grid:blockDraw(block,i,j)
        local tile = self.tiles[ self.map[i][j].btype ]
				local power = self.map[i][j].power
        if(block.btype == BLOCK_POWER) then
          if(block.capacity < 1)then
            love.graphics.setColor( 100, 100, 100, 200 )
            
          end
          love.graphics.draw(tile, (i - 1)*self.cw , (j - 1)*self.ch)
          
          love.graphics.setColor( 255, 255, 255, 255 )
          love.graphics.printf(block.capacity, (i - 1)*self.cw , (j - 1)*self.ch , 32, "left")
        else
          love.graphics.draw(tile, (i - 1)*self.cw , (j - 1)*self.ch)
        end
end

function grid.makeBlock(blocktype)
  local c = -1;
  if blocktype == BLOCK_POWER then
    c = 6
  end

  return {
    btype = blocktype;
    power = 0;
    capacity = c;
    dirx = 0;
    diry = 0;
    action = "";
  };
  
end

function grid:init()
  self.height = 25
	self.width = 25
	self.ch = 32
	self.cw = 32
  self.hlen = self.height * self.ch
	self.wlen = self.width * self.cw
	self.offs = {}
	self.offs.h = 10
	self.offs.w = 10
  self.showcells = true
  
  -- текущий тип блока в конструкторе
	self.currentBlockType = 0
  
	self.map = {}
  self:fill()
  
	self.newmap = {}
  self:clearNewMap()
    
  self.backup = {} -- to save map before simulation
  self.state = 0 -- 0 - construction, 1 - simulation
	
  self.workArea = {
    x = 0,
    y = 19,
    w = 25,
    h = 6,
    enable = true
    }
 
  self.tiles = gfx.tiles
	
	self.activators = {}
end

function grid:backup()
  	for i = 1, self.width do
		self.backup[i] = {}
		for j = 1, self.height do
			self.backup[i][j] = self.makeBlock(self.map[i][j].btype) -- Fill the values here
		end
	end
end
    
-- заполняем всё поле блоками с типом 0, т.е. пустыми
function grid:fill()
	for i = 1, self.width do
		self.map[i] = {}
		for j = 1, self.height do
			self.map[i][j] = self.makeBlock(0) -- Fill the values here
		end
	end
end

-- Очищаем второй экран
function grid:clearNewMap()
	for i = 1, self.width do
		self.newmap[i] = {}
		for j = 1, self.height do
			self.newmap[i][j] = self.makeBlock(0) -- Fill the values here
		end
	end
end
-- рисуем сетку
function grid:drawGridCells()	
  if self.showcells then
    love.graphics.setColor( 0, 92, 119, 150 )
    for i = 0, self.height do
        love.graphics.line(0, i*self.cw, self.wlen, i*self.cw)
    end
    
    for j = 0, self.width do	
        love.graphics.line( j*self.cw, 0, j*self.cw, self.hlen)
    end
    love.graphics.setColor( 255, 255, 255 )
  end
end


function grid:drawWorkArea()
  love.graphics.setColor( 200, 255, 200, 100 )
  local x = self.workArea.x * self.cw
  local y = self.workArea.y * self.ch
  local w = self.workArea.w * self.cw
  local h = self.workArea.h * self.ch
  love.graphics.printf("Work area", x, y , 100, "left")
  love.graphics.rectangle( "line", x, y, w, h )
end
  
  
function grid:drawTiles()
    -- draw tiles
  love.graphics.setColor( 255, 255, 255, 255 )
	for i = 1, self.width do
		for j = 1, self.height do
			if self.map[i][j].btype ~= 0 then
				--local tile = self.tiles[ self.map[i][j].btype ]
				--local power = self.map[i][j].power
				--love.graphics.draw(tile, (i - 1)*self.cw , (j - 1)*self.ch)
        grid:blockDraw(self.map[i][j],i,j)
				--if power == 1 then
				--	love.graphics.draw(self.tiles[BLOCK_POWERED_MASK], (i - 1)*self.cw , (j - 1)*self.ch)
				--end
			end
		end
	end
end

-- Рисуем все тайлы с карты and workArea
function grid:draw()
    grid:drawWorkArea()
    grid:drawTiles()
end

-- сохранение карты в файл
function grid:save(filename)

-- Opens a file in append mode
file = io.open(filename, "w")

-- sets the default output file as test.lua
io.output(file)

	for i = 1, self.width do
		for j = 1, self.height do
				--local tile = self.tiles[ self.map[i][j].btype ]
				local power = self.newmap[i][j].power
				io.write( power.." " )
		end
		io.write("\n")
	end
	
-- closes the open file
io.close(file)

end

-- рекурсивно запитываем все элементы
function grid:powerOn(x,y)
	if x < 1 then return; end
	if y < 1 then return; end
	if x > self.width then return; end
	if y > self.height then return; end
	if self.map[x][y].btype == BLOCK_EMPTY then return; end
	if self.map[x][y].power == 1 then return; end
  
	self.map[x][y].power = 1;
  
	if (self.map[x][y].btype == BLOCK_POWER) or self.map[x][y].btype == BLOCK_WIRE  then
		local ym = self:powerOn(x, y-1)
		local yp = self:powerOn(x, y+1)
		local xm = self:powerOn(x-1, y)
		local xp = self:powerOn(x+1, y)
    return(ym or yp or xm or xp);
	end
  
  return(1);
end

-- назначить блоку действие(которе выполнится при активации)
function grid:assingAction(x,y,act)
	if x < 1 then return; end
	if y < 1 then return; end
	if x > self.width then return; end
	if y > self.height then return; end
	self.map[x][y].action = act

end

function grid:assingActionsAround(x,y,act)
  for i = -1, 1 do
			for j = -1, 1 do
				if grid.map[x+i][y+j].btype ~= 0 then
					self:assingAction(x+i,y+j,act)
				end
			end
		end
end

function grid:assingActionsToNeighbours(x,y,act)
	if x < 1 then return; end
	if y < 1 then return; end
	if x > self.width then return; end
	if y > self.height then return; end
	if self.map[x][y].btype == BLOCK_EMPTY  
    then return; end
	if self.map[x][y].action ~= "" then return; end
  
  self:assingAction(x,y,act);
  
  grid:assingActionsToNeighbours(x + 1,y,     act)
  grid:assingActionsToNeighbours(x - 1,y,     act)
  grid:assingActionsToNeighbours(x    ,y + 1, act)
  grid:assingActionsToNeighbours(x    ,y - 1, act)
end



-- назначить действие всем блокам вокруг (x,y) 
function grid:assingActions(x,y)
	if self.map[x][y].btype == BLOCK_MOVEUP then
		--grid:assingActionsAround(x,y,"up")
    grid:assingActionsToNeighbours(x,y,"up")
	end
  
	if self.map[x][y].btype == BLOCK_MOVELEFT then
		grid:assingActionsAround(x,y,"left")
	end
  
	if self.map[x][y].btype == BLOCK_MOVERIGHT then
		grid:assingActionsAround(x,y,"right")
	end
end

-- запитываем все элементы
function grid:activation()
	for i = 1, self.width do
		for j = 1, self.height do
			if grid.map[i][j].btype == BLOCK_POWER then
            if(grid.map[i][j].power==0 and self.map[i][j].capacity > 0)then             
              
              if (grid:powerOn(i,j) ~= nil) then
                self.map[i][j].capacity = self.map[i][j].capacity - 1
              end
              
            end
			end
		end
	end
	for i = 1, self.width do
		for j = 1, self.height do
				if grid.map[i][j].power == 1 then
					grid:assingActions(i,j)
				end
		end
	end	
end

-- отключаем питание у всех элементов
function grid:deActivateAll()
	for i = 1, self.width do
		for j = 1, self.height do
			 self.map[i][j].power = 0;
		end
	end
end

-- выполняем действие для блока (i,j)
function grid:doAction(i,j)

  if self.map[i][j].action=="up" then
	    self.newmap[i][j] = self.makeBlock(0)
      self.newmap[i][j - 1] = self.map[i][j]
      self.newmap[i][j - 1].act = ""
      self.newmap[i+1][j].capacity = self.map[i][j].capacity
      return;
	end 
  
  if self.map[i][j].action=="left" then
	    self.newmap[i][j] = self.makeBlock(0)
      self.newmap[i-1][j] = self.map[i][j]
      self.newmap[i-1][j].act = ""
      self.newmap[i+1][j].capacity = self.map[i][j].capacity
      return;
	end 
  
  if self.map[i][j].action=="right" then
	    self.newmap[i][j] = self.makeBlock(0)
      self.newmap[i+1][j] = self.map[i][j]
      self.newmap[i+1][j].act = ""
      self.newmap[i+1][j].capacity = self.map[i][j].capacity
      return;
	end 
  
  self.newmap[i][j] = self.map[i][j]
end

-- выполняем действия для всех элементов
function grid:action()
	--self.newmap = {}
  self:clearNewMap()
	for i = 1, self.width do
	self.newmap[i] = {}
		for j = 1, self.height do
			self.map[i][j].power = 0
			if self.map[i][j].btype == 0 then
				self.newmap[i][j] = self.map[i][j]
			else
				self:doAction(i,j)
			end
		end
	end
	
	for i = 1, self.width do
	for j = 1, self.height do
		self.map[i][j] = self.newmap[i][j]
		self.map[i][j].power = 0
		self.map[i][j].action = ""
    
	end
	end
end

function grid:update()
		--self:activation()
		--self:action()
end

function grid:copyForm(m)
	for i = 1, self.width do
		self.map[i] = {}
		for j = 1, self.height do
			self.map[i][j] = self.makeBlock(m[i][j].btype) -- Fill the values here
		end
	end
end

-- возвращает координаты блока под курсором
function grid:pickCell()
		local xc = math.floor(love.mouse.getX() / self.cw) + 1;
		local yc = math.floor(love.mouse.getY() / self.ch) + 1;
		if grid.workArea.enable and (xc <= grid.workArea.x or yc <= grid.workArea.y) then
      return nil;
    end
		if xc <= self.width and yc <= self.height then
			return { x = xc, y = yc }
		end
end

return(grid)