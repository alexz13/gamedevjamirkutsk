
useDebugConsole = false

suit = require('libs.SUIT'); -- для textpanel и menu 
Gamestate = require('libs.hump.gamestate'); 
--require('libs.console.loveconsole') -- debug console
if useDebugConsole then
  require("libs.lovedebug")
end

--Example
require('src.print');
require('src.resources'); -- gfx,snd
require("src.grid");
require('src.interface');
require('src.textpanel');
require('src.gamesaver');
require('src.networking');

--  game states
require('src.states.constructor');
require('src.states.menu');
require('src.states.savelevel');
require('src.states.intro');

function love.load(arg)
  if not(useDebugConsole)then
    -- add this for debugging in zbstudio
    if arg[#arg] == "-debug" then require("mobdebug").start(); end
  end
  
  --networking:test()
  
	love.window.setTitle("Space Kite")
	love.window.setMode( 800,900, {resizable=false, vsync=false} )
	
  gfx:init()
	snd:init()
  
  gamesaver:init()
      -- only register draw, update and quit
  Gamestate.registerEvents()
  --Gamestate.switch(constructor)
  Gamestate.switch(menu)
  --Gamestate.switch(loveconsole)
  --Gamestate.switch(intro)
end
