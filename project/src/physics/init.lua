phy = {}

function phy:init()
  phy.active = false
  love.physics.setMeter(64)
  --phy.world = love.physics.newWorld(0, 9.81*64, false)
  phy.world = love.physics.newWorld(0, 0, false)
  phy.world:setCallbacks(phy.beginContact, phy.endContact, phy.preSolve, phy.postSolve)
  phy.objects = {}
  phy.removals = {}
  phy.ondraw = function (self)
    local fl = self.body:getFixtureList( )
    for i = 1,#fl do
      local shape = fl[i]:getShape( )
      local block = fl[i]:getUserData()
      if shape:getType() == 'circle' then
        love.graphics.circle("line", self.body:getX(),self.body:getY(), shape:getRadius(), 20)
      else
        if(block.blocktype == nil)then
          love.graphics.polygon("line", self.body:getWorldPoints(shape:getPoints()))
        else
          local rot = self.body:getAngle()
          local x0,y0,x1,y1,x2,y2,x3,y3 = self.body:getWorldPoints(shape:getPoints())
          local img = resources.img.tiles[block.blocktype]
          love.graphics.draw(img,x0,y0,rot)
          --love.graphics.polygon("line", self.body:getWorldPoints(shape:getPoints()))
        end
      end
    end
  end
end


function phy:removeObject(obj)
  table.insert(removals, obj)
end

function phy:clearWorld()
  phy.world:destroy()
  collectgarbage()
--  for i = 1,#phy.objects do 
--    if(phy.objects[i].blocktype ~= nil)then
--      phy.objects[i] = nil
      
      
--    end
--  end
  phy:init()
  
end

function phy:newObject(x,y,bodytype)
  phy.objects[#phy.objects + 1] = {
    body =  love.physics.newBody(phy.world, x, y, bodytype),
    draw = phy.ondraw
  }
  return phy.objects[#phy.objects]
end

function phy:addFixture(obj, shape, density, restitutuion)
  local f = love.physics.newFixture(obj.body, shape, density)
  f:setRestitution(restitutuion)
  f:setUserData(obj)
  return(f)
end


function phy:update(dt)
  if phy.active then
    phy:applyGravity()
    phy.world:update(dt)
--    for _, remobj in ipairs(removals) do
--      remobj:destroy()
--    end
  end
end


function phy:gravityForce(a,b) 
  local ax,ay = a.body:getX(), a.body:getY()
  local bx,by = b.body:getX(), b.body:getY()
  
  local dsqr = ((bx-ax)^2 + (by-ay)^2)^(1.5)
  
  if dsqr == 0 then
      return 0,0
  end
  -- (dsqr * math.sqrt(dsqr))
  local Gmm = 6700 * a.body:getMass() * b.body:getMass()
  local fx = Gmm * ( bx - ax ) / (dsqr)
  local fy = Gmm * ( by - ay ) / (dsqr)
  return fx, fy
end

function phy:applyGravity()
    for i = 1,#phy.objects do
    local fx,fy = 0,0
      for j = 1,#phy.objects do
        if phy.objects[j].body:getType()~='static' then
          local x,y = phy:gravityForce(phy.objects[i],phy.objects[j])
          fx = fx + x
          fy = fy + y
        end
      end
      phy.objects[i].body:applyForce( fx,fy )
    end
end

function phy:makeStaticBoundaries(w,h) 
  local obj = phy:newObject( 0, 0, 'static')
  phy:addFixture(obj,
                 love.physics.newRectangleShape(w/2, 0, w, 2),
                 5,
                 0)
  phy:addFixture(obj,
                 love.physics.newRectangleShape(w/2, h, w, 2),
                 5,
                 0)
  phy:addFixture(obj,
                 love.physics.newRectangleShape(0, h/2, 2, h),
                 5,
                 0)
  phy:addFixture(obj,
                 love.physics.newRectangleShape(w, h/2, 2, h),
                 5,
                 0)
end


function phy.beginContact(a, b, coll)
     x,y = coll:getNormal()
    --print("object "..a:getUserData().." colliding with "..b:getUserData().." with a vector normal of: "..x..", "..y)
end

function phy.endContact(a, b, coll)
    --print("object "..a:getUserData().." uncolliding with "..b:getUserData())
end

function phy.preSolve(a, b, coll)
end

function phy.postSolve(a, b, coll, normalimpulse, tangentimpulse)
 
end

return phy