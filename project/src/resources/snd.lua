local snd = {}

function snd:load()
    self.bgSndSrc = love.audio.newSource("snd/sctheme4ost.ogg", "stream")
	  self.bgSndSrc:setLooping(true)
end

function snd:play()
	  love.audio.play(self.bgSndSrc)
end

function snd:stop()
    love.audio.stop(self.bgSndSrc)
end

return(snd)