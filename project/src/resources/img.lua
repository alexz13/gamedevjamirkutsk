local mech = require("src.mech");
local img = {
  tiles = {},
  tileh = 32,
  tilew = 32
   
}

function img:load()

  self.tiles[mech.BLOCK_EMPTY] = love.graphics.newImage("img/0.png")
	self.tiles[mech.BLOCK_POWER] = love.graphics.newImage("img/1.png")
	self.tiles[mech.BLOCK_WIRE] = love.graphics.newImage("img/4.png")
	self.tiles[mech.BLOCK_MOVEUP] = love.graphics.newImage("img/3.png")
  
	self.tiles[mech.BLOCK_MOVEDOWN] = love.graphics.newImage("img/3d.png")
	self.tiles[mech.BLOCK_MOVELEFT] = love.graphics.newImage("img/3l.png")
	self.tiles[mech.BLOCK_MOVERIGHT] = love.graphics.newImage("img/3r.png")
  
  self.tiles[mech.BLOCK_POWERED_MASK] = love.graphics.newImage("img/tileActive.png")
  
	self.tiles[mech.BLOCK_SEPARATOR] = love.graphics.newImage("img/2.png")
	self.tiles[mech.BLOCK_LOCATOR] = love.graphics.newImage("img/5.png")
	self.tiles[mech.BLOCK_CONTAINER] = love.graphics.newImage("img/10.png")
  
  self.tiles[mech.BLOCK_ASTEROID] = love.graphics.newImage("img/asteroid.png")
  
  self.biglogo = love.graphics.newImage("img/logo.png")
  self.bgimage = love.graphics.newImage("img/andromeda.jpg")
  self.missionSampleImage = love.graphics.newImage("img/bg0.png")
  
end

function img:getBlockTile(tilename)
  return(self.tiles[mech[tilename]])
end

return(img)
