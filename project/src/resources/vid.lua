local vid = { }

function vid:load()

  self.video = love.graphics.newVideo("video/small.ogv", true)
  width, height, flags = love.window.getMode( )
  self.videoProperies = { 
    x = (width - self.video:getWidth())/2,
    y = self.video:getHeight()/2,
    w = self.video:getWidth(),
    h = self.video:getHeight()
  }
  --self.video:play()
end

function vid:update(dt)
  --if not (self.video:isPlaying()) then
  --  self.video:rewind()
  --end
end

function vid:draw()
  love.graphics.draw(self.video, self.videoProperies.x, self.videoProperies.y)
end

return vid