local BASE = (...) .. "."

local resources = {
	img = require(BASE .. "img"),
	snd = require(BASE .. "snd"),
	fnt = require(BASE .. "fnt"),
  vid = require(BASE .. "vid")
}

function resources:load()
  self.img:load()
  self.snd:load()
  self.fnt:load()
  self.vid:load()
end

return resources