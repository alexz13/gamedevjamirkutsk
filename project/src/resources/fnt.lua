local fnt = {}

function fnt:load()
  fnt.unifont = love.graphics.newFont( "fnt/unifont-8.0.01.ttf", 16 )
  fnt.unifontBold = love.graphics.newFont( "fnt/unifont-8.0.01.ttf", 24 )
end

return(fnt)