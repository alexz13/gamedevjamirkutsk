 local constructor = {}
 
function constructor:init()

end

function constructor:enter(previous) -- runs every time the state is entered
    interface.interfacePanel.visible = true
end

function constructor:leave()

end

function constructor:keypressed( key, scancode, isrepeat )
  if scancode=='escape' then
    states:push('menu')
  end
end

function constructor:update(dt)
  
   local is_down_m = love.mouse.isDown(1)
   if is_down_m then

   end
  interface:update(dt)
end

function constructor:draw()
  love.graphics.setColor( 255, 255, 255 )
	love.graphics.draw(resources.img.bgimage, 0, 0)
	interface:draw()
end

return(constructor)