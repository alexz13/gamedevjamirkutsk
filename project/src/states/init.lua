local BASE = (...) .. "."

local states = {
	gamestate = require('libs.hump.gamestate'); 
	constructor = require(BASE .. "constructor"),
	intro = require(BASE .. "intro"),
	menu = require(BASE .. "menu"),
  test = require(BASE .. "test"),
	savelevel = require(BASE .. "savelevel"),
  phyTest = require(BASE .. "phyTest"),
}

function states:init()
	states.gamestate.registerEvents()
--	self.gamestate.switch(self.menu)
end

function states:switch(statename)
  local state = states[statename]
	states.gamestate.switch(state)
end

function states:pop()
	states.gamestate.pop()
end

function states:push(statename)
	states.gamestate.push(states[statename])
end

return states