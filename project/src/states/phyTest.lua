local phyTest = {}

function phyTest:init()
  interface.interfacePanel.visible = true
  interface.grid.visible = true
  interface.screenCamera.showcameraScrollingRanges = true

  local w,h = resources.img.bgimage:getDimensions()
  phy:makeStaticBoundaries(w,h) 


  local obj = phy:newObject( 650/2, 650/2, 'dynamic')
  phy:addFixture(obj,
                 love.physics.newCircleShape(20),
                 1,
                 0.9)
 
   local obj = phy:newObject( 200, 550, 'dynamic')
  phy:addFixture(obj,
                 love.physics.newRectangleShape(0, 0, 50, 100),
                 1,
                 0.1)
  phy:addFixture(obj,
                 love.physics.newRectangleShape(50, 0, 20, 20),
                 1,
                 0.1)
               
   local obj = phy:newObject(200, 400, "dynamic")
  phy:addFixture(obj,
                 love.physics.newRectangleShape(0, 0, 100, 50),
                 1,
                 0.1)
  phy.active = true
end
 
 
function phyTest:update(dt)
  interface:update(dt)
  --phy.world:update(dt) --this puts the world into motion
  phy:update(dt)
  --here we are going to create some keyboard events
  if love.keyboard.isDown("right") then --press the right arrow key to push the ball to the right
    phy.objects[2].body:applyForce(400, 0)
  elseif love.keyboard.isDown("left") then --press the left arrow key to push the ball to the left
    phy.objects[2].body:applyForce(-400, 0)
  elseif love.keyboard.isDown("up") then --press the up arrow key to set the ball in the air
    phy.objects[2].body:applyForce(0, -400)
    --potentially large velocity generated by the change in position
  elseif love.keyboard.isDown("down") then --press the up arrow key to set the ball in the air
    phy.objects[2].body:applyForce(0, 400) --we must set the velocity to zero to prevent a potentially large velocity generated by the change in position
  end
  
end


function phyTest:quit()

end
 
function phyTest:draw()
      interface.screenCamera:attach()
        love.graphics.setColor( 255, 255, 255 )
        love.graphics.draw(resources.img.bgimage, 0, 0)
        love.graphics.setColor(255, 255, 255)
        for i = 1, #phy.objects do
            phy.objects[i]:draw()
        end
    interface.screenCamera:detach()   
    
end

function phyTest:wheelmoved(x, y)
  interface.screenCamera:wheelmoved(x, y)
end

return(phyTest)
