local intro = {}

function intro:init()
  --intro.video = love.graphics.newVideo("video/small.ogv",true)
  width, height, flags = love.window.getMode( )
  intro.videoProperies = { 
    x = (width - intro.video:getWidth())/2,
    y = intro.video:getHeight()/2,
    w = intro.video:getWidth(),
    h = intro.video:getHeight()
  }
  intro.video:play()
end

function intro:update(dt)
  --if not (intro.video:isPlaying()) then
  --  intro.video:rewind()
  --end
end

function intro:draw()
    love.graphics.draw(intro.video, intro.videoProperies.x, intro.videoProperies.y)
end

function intro:exitToMenu()
  intro.video:pause()
  intro.video = {}
  collectgarbage()
  Gamestate.switch(menu)
end

function intro:keypressed( key, scancode, isrepeat )
  intro:exitToMenu()
end

return(intro)