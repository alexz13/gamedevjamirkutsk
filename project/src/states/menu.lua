local menu = {}
menu.paused = false

function menu:init() 
  resources.snd:play()
  
  interface.mainmenu.onPlay = function()
    states:switch("constructor")
  end
  
  interface.mainmenu.onLevel = function()
    interface.messagePanel.visible = true
  end
  
  interface.mainmenu.onExit = function()
    love.event.quit()
  end
  
end

function menu:enter(previous, ...)
  interface.mainmenu.visible = true
  if previous == constructor then
    menu.paused = true
  else
    menu.paused = false
  end
end

function menu:leave()
  interface.mainmenu.visible = false
end

function menu:update(dt)
  interface:update(dt)
end

function menu:draw()
  interface:draw()
end

function menu:keypressed( key, scancode, isrepeat )
  if scancode=='escape' then
    if menu.paused then
      return states.pop() 
    end
  end
end

return(menu)