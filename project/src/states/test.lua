local test = {}

function test:init()
  interface.interfacePanel.visible = true
  interface.grid.visible = true
    local w,h = resources.img.bgimage:getDimensions()
  phy:makeStaticBoundaries(w,h) 
end

function test:update(dt)
  interface:update(dt)
  phy:update(dt)
end

function test:draw()
  -- draw scrollable content
    interface.screenCamera:attach()
        love.graphics.setColor( 255, 255, 255 )
        love.graphics.draw(resources.img.bgimage, 0, 0)
        for i = 1, #phy.objects do
            phy.objects[i]:draw()
        end
    interface.screenCamera:detach()   
  -- draw non-scrollable content
    interface:draw()        
end

function test:exitToMenu()
--  collectgarbage()
--  states.switch(menu)
end

function test:keypressed( key, scancode, isrepeat )
--  test:exitToMenu()
  if scancode=='space' then
    local groups = interface.grid:selectBlockGroups()
    local groupCenters = interface.grid:getBlockGroupCenters(groups)
    for i = 1,#groups do
      local gx,gy = groupCenters[i].x,groupCenters[i].y
      local obj = phy:newObject( gx, gy, 'dynamic')   
      for j = 1,#groups[i] do
        local x,y = interface.grid.cw*(groups[i][j].x - 0.5) - gx,
                    interface.grid.ch*(groups[i][j].y - 0.5) - gy
          --obj.blocktype = groups[i][j].blocktype
          local fixture = phy:addFixture(obj,
                       love.physics.newRectangleShape(x, y, interface.grid.cw, interface.grid.ch),
                       1,
                       0.9)
          fixture:setUserData(groups[i][j])
          groups[i][j].gid = 0
      end
    end
    phy.active = true
    interface.grid.visible = false
  end
  if scancode=='escape' then
    phy.active = false
    phy:clearWorld()
    interface.grid.visible = true
  end
end

function test:wheelmoved(x, y)
  interface.screenCamera:wheelmoved(x, y)
end
  
function test:mousepressed(x, y, button, istouch)
    interface.grid.mousepressed(x, y, button, istouch)
end

function test:mousemoved( x, y, dx, dy, istouch )
  local wx,wy = interface.screenCamera:getWorldMousePosition()
  love.window.setTitle(wx .. ' , ' .. wy)
end

  
return(test)