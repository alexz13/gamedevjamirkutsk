local BASE = (...) .. "."

local interface = {
  -- панель инструментов для конструктора
	interfacePanel = require(BASE .. "interfacePanel"),
  -- окно с сообщением
  messagePanel = require(BASE .. "textpanel"),
  -- главное меню
  mainmenu = require(BASE .. "mainmenu"),
  -- конструктор/симулятор
	grid = require(BASE .. "grid"),
  --загрузка файлов
	--filePanel = require(BASE .. "filebox"),
  suit = require("libs.SUIT"),
  screenCamera = require(BASE .. "screenCamera")
}

function interface:init()
        -- добавляем кастомные настройки GUI-библиотеки interface.suit
  interface.suit.theme.color = {
    normal  = {bg = { 66, 86, 160,180}, fg = {188,188,188,255}},
    hovered = {bg = { 50,153,187,200}, fg = {255,255,255,255}},
    active  = {bg = {55,123, 205,200}, fg = {225,225,225,255}}
  }

  self.messagePanel:init()
  self.interfacePanel:init()
  
  bgW, bgH =  resources.img.bgimage:getDimensions() 
    -- Задаём ракурс камеры в пикселах (по центру фонового рисунка) 
  self.screenCamera:init(bgW/2, bgH/2,1)
    -- Задаём величину игрового поля в пикселах (по размеру фонового рисунка) 
  self.screenCamera:setDimensions(bgW,bgH)
  
  self.grid:init()
  interface:onResize()
end

function interface:onResize()
  self.interfacePanel:onResize()
  self.screenCamera:onResize()
  
  local width, height = love.graphics.getDimensions()
    -- зона действия по клику
  self.workingArea = utils.newArea(0,
                                   0,
                                   width,
                                   height - interface.interfacePanel.panelh)
end

function interface:draw()
  if self.mainmenu.visible then
    self.mainmenu:draw()
  end
  if self.grid.visible then
    self.screenCamera:attach()
    self.grid:draw()
    self.screenCamera:detach()
  end
  if self.interfacePanel.visible then
    self.interfacePanel:draw()
  end
  if self.messagePanel.visible then
    self.messagePanel:draw()
  end
    self.screenCamera:draw()
end

function interface:update(dt)
  if self.mainmenu.visible then
    self.mainmenu:update(dt)
  end
  if self.interfacePanel.visible then
    self.interfacePanel:update(dt)
  end
  if self.messagePanel.visible then
    self.messagePanel:update(dt)
  end
  self.screenCamera:update(dt)
end

return interface