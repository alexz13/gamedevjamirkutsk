local grid = {}


function grid:blockDraw(block,i,j)
        local tile = self.tiles[ self.map[i][j].blocktype ]
        love.graphics.draw(tile, (i - 1)*self.cw , (j - 1)*self.ch)
        love.graphics.printf(block.gid, (i - 1)*self.cw , (j - 1)*self.ch , 32, "left")
end

function grid:makeBlock(btype)
  grid.blocks[ #grid.blocks  + 1] = {
    blocktype = btype;
    x = -1,
    y = -1,
    mass = 1;
  }
  return self.blocks[#self.blocks];
end

-- заполняем всё поле блоками с типом 0, т.е. пустыми
function grid:fill()
	for i = 1, self.width do
		self.map[i] = {}
		for j = 1, self.height do
			self.map[i][j] = { x = i, y = j, gid = -1 } -- Fill the values here
		end
	end
end

function grid:init()
  self.visible = false;
  
  -- размеры блока
	self.ch = 32
	self.cw = 32
  
  -- количество блоков
  --self.height = 25
	--self.width = 25
  self.width = math.floor(interface.screenCamera.cameraRanges:width()/self.cw)
  self.height = math.floor(interface.screenCamera.cameraRanges:height()/self.ch)
  -- длина и ширина поля в пикселах
  self.hlen = self.height * self.ch
	self.wlen = self.width * self.cw

                            
	self.offs = {}
	self.offs.h = 10
	self.offs.w = 10
  
  self.showcells = true
   
  self.blocks = {} 
   
	self.map = {}
  self:fill()
  
  self.state = 0 -- 0 - construction, 1 - simulation
	
  self.workArea = {
    x = 0,
    y = 0,
    w = math.floor(interface.screenCamera.cameraRanges:width()/self.cw),
    h = math.floor(interface.screenCamera.cameraRanges:height()/self.ch),
    enable = true
    }
 
  self.tiles = resources.img.tiles
	
	self.activators = {}
end


-- рисуем сетку
function grid:drawGridCells()	
  if self.showcells then
    love.graphics.setColor( 0, 92, 119, 100 )
    for i = 0, self.height do
        love.graphics.line(0, i*self.cw, self.wlen, i*self.cw)
    end
    
    for j = 0, self.width do	
        love.graphics.line( j*self.cw, 0, j*self.cw, self.hlen)
    end
    love.graphics.setColor( 255, 255, 255 )
  end
end

  
function grid:drawTiles()
    -- draw tiles
  love.graphics.setColor( 255, 255, 255, 255 )
	for i = 1, self.width do
		for j = 1, self.height do
			if self.map[i][j] ~= nil then
        if self.map[i][j].blocktype ~= nil then      
          grid:blockDraw(self.map[i][j],i,j)
        end
			end
		end
	end
end

-- Рисуем все тайлы с карты and workArea
function grid:draw()
    --grid:drawWorkArea()
    grid:drawTiles()
    if(self.showcells)then
      grid:drawGridCells()	
    end
end


function grid:update(dt)
  
end

function grid.mousepressed(x, y, button, istouch)
  if interface.workingArea:isInside(x,y) then
    local mx,my = interface.screenCamera:getWorldMousePosition()
    local b = grid:pickCell(mx,my)
    if(b ~= nil)then
--      interface.grid.map[b.x][b.y] = {
--        blocktype = interface.interfacePanel.currentBlockType,
--        gid=-1
--      }
        interface.grid.map[b.x][b.y].blocktype = interface.interfacePanel.currentBlockType
        interface.grid.map[b.x][b.y].gid = 0
    end
  end
end

-- возвращает координаты блока под курсором
function grid:pickCell(mx,my)
		local xc = math.floor(mx / self.cw) + 1;
		local yc = math.floor(my / self.ch) + 1;
		--if grid.workArea.enable and (xc <= grid.workArea.x or yc <= grid.workArea.y) then
    --  return nil;
    --end
		if xc <= self.width and yc <= self.height then
			return { x = xc, y = yc }
		end
end

--Recursive 4-way floodfill, crashes if recursion stack is full
function grid:recursiveSelect(x,  y, gid, grouplist)
  if( x<1 or x>self.width or y<1 or y>self.height)then
    return
  end
  if(self.map[x][y] == nil)then
    return
  end
  if(self.map[x][y].gid == nil)then
    return
  end
  
  if(self.map[x][y].gid == 0)then
    self.map[x][y].gid = gid; 
    grouplist[gid][#grouplist[gid] + 1] = self.map[x][y]
    grid:recursiveSelect(x + 1, y    , gid, grouplist);
    grid:recursiveSelect(x - 1, y    , gid, grouplist);
    grid:recursiveSelect(x    , y + 1, gid, grouplist);
    grid:recursiveSelect(x    , y - 1, gid, grouplist);
  end
end

-- разобъём смежные блоки на группы с помощью алгоритма заливки
function grid:selectBlockGroups()
  local gid = 1;
  local grouplist = {}
	for i = 1, self.width do
		for j = 1, self.height do
			if self.map[i][j] ~= nil then
        if self.map[i][j].blocktype ~= nil then    
          if(self.map[i][j].gid == 0)then
            grouplist[gid] = {}
            grid:recursiveSelect(i,  j, gid, grouplist)
            gid = gid +1
          end
        end
			end
		end
	end
  return(grouplist)
end

-- получим центр группы блоков
function grid:getBlockGroupCenters(groups)
  local groupCenters = {}
    for i = 1,#groups do  
      local xc, yc = 0 , 0
      for j = 1,#groups[i] do
        xc = xc + self.cw * (groups[i][j].x - 1.5) 
        yc = yc + self.ch * (groups[i][j].y - 1.5)
      end
      
      groupCenters[i] = { x = xc ,
                          y = yc
                        }
                        
    end
    return(groupCenters)
end

return(grid)