local filebox = {}

function filebox:init()
  filebox.directory = '/skite'
  filebox.filecount = 0
  filebox.rows = 5
  filebox.pages = 1
  filebox.page = 1
  filebox.rowsOffs = 0

  filebox.x = 300
  filebox.y = 300
  filebox.mx = 3
  filebox.my = 3

  filebox.files={}
  filebox.buttonW = 200
  filebox.buttonH = 24
  
  filebox.input = { text = "test" }
  
  filebox.buttonSize = suit.layout:row(filebox.buttonW,filebox.buttonH)
  self.width = filebox.buttonW + 2*filebox.mx
  self.height = (filebox.rows + 4) *filebox.buttonH + (filebox.rows + 1 )*filebox.my
  
end

function filebox:previousPage()
    if (self.page>1) then self.page = self.page - 1; end
end

function filebox:nextPage()
    if (self.page<filebox.pages) then self.page = self.page + 1; end
end
  
function filebox:scanDirectory()
  self.files = love.filesystem.getDirectoryItems(filebox.directory)
  for k, file in ipairs(self.files) do
       print(k .. ". " .. file)
       self.filecount = self.filecount + 1
  end
  filebox.pages = math.floor(filebox.filecount / filebox.rows + 0.5)
end

function filebox:update(dt)
  suit.layout:reset(self.x, self.y , self.mx, self.my)
    if suit.Input(filebox.input, suit.layout:row(filebox.buttonW,filebox.buttonH)).submitted then
        print(filebox.input.text)
  end
 suit.Label("Levels", {align='left'}, suit.layout:row(filebox.buttonW,filebox.buttonH*2/3))
 
 filebox.rowsOffs = (filebox.page - 1)*filebox.rows
    
  if filebox.filecount > filebox.rows then
     for i = 1, filebox.rows do
          if suit.Button(filebox.files[filebox.rowsOffs + i], suit.layout:row(filebox.buttonW,filebox.buttonH)).hit then
             filebox.input.text = filebox.files[filebox.rowsOffs + i]
          end
     end
  else
     for i = 1, filebox.filecount do
          if suit.Button(filebox.files[i], suit.layout:row(filebox.buttonW,filebox.buttonH)).hit then
             filebox.input.text = filebox.files[i]
          end
     end
     local ex = ''
     for i = (filebox.filecount + 1), filebox.rows do
          ex = ex..' '
          suit.Button(ex, suit.layout:row(filebox.buttonW,filebox.buttonH));
     end
  end
  
  suit.layout:reset(self.x, self.y + self.height - 1.5 * filebox.buttonH , self.mx, self.my)
  suit.Button("LOAD", suit.layout:col(filebox.buttonW/2 - self.mx ,filebox.buttonH));
  suit.Button("SAVE", suit.layout:col(filebox.buttonW/2 - self.mx ,filebox.buttonH));
  
  suit.layout:reset(self.x - 20 - self.mx, self.y , self.mx, self.my)
  if(suit.Button("<", suit.layout:row(20, self.height)).hit) then
    filebox:previousPage()
  end
  
  suit.layout:reset(self.x + self.width - self.mx , self.y , self.mx, self.my)
  if(suit.Button(">", suit.layout:row(20, self.height)).hit) then 
    filebox:nextPage()
  end
  
  suit.layout:reset(self.x, self.y + self.height + 4 * filebox.buttonH , self.mx, self.my)
end

function filebox:draw()
    love.graphics.setColor( 1, 52, 75, 255 )
    love.graphics.rectangle( 'fill', self.x - self.mx, self.y, self.width , self.height  )
end

return(filebox)
