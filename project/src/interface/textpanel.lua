local textpanel = {}

unifont = {}
unifontBold = {}
  
function textpanel:init()
  --unifont = love.graphics.newFont( "fnt/unifont-8.0.01.ttf", 16 )
  --unifontBold = love.graphics.newFont( "fnt/unifont-8.0.01.ttf", 24 )
  love.graphics.setFont(resources.fnt.unifont)
  self.x = 100
  self.y = 100
  self.h = 400
  self.w = 400
  self.visible = false
  self.pic = resources.img.missionSampleImage
  
  self.margw = 10
  self.margh = 40
  self.margHCaption = 8
  self.text = [[Привет, юный друг! Сегодня далёкие галактики как никогда нуждаются в твоей помощи. Чтобы всё было круто, тебе нужно сделать очень цепляющую игру. И как можно более качественную. В добрый путь! А ещё нужен нормальный шрифт, а то ШГ.]]
  self.caption = 'Текущее задание'
end

function textpanel:onNextButton()
  self.visible = false
end
  
function textpanel:update()
  if self.visible then
    -- put the layout origin at specific position 
    -- cells will grown down and to the right from this point
    -- also set cell padding to 20 pixels to the right and to the bottom
    interface.suit.layout:reset(self.x + self.margw, self.y + self.h - self.margh - 30 , 20,20)

    -- put a button at the layout origin
    -- the cell of the button has a size of 200 by 30 pixels
    if interface.suit.Button("ЗАКРЫТЬ", interface.suit.layout:row(self.w - 2*self.margw,30)).hit then
      textpanel:onNextButton()
    end
  end
end


function textpanel:draw()
  if self.visible then
    -- border
    love.graphics.setColor( 0, 72, 99, 255 )
    love.graphics.rectangle( "line", self.x, self.y, self.w, self.h, 8, 8, 5  )
    love.graphics.setColor( 0, 52, 79, 200 )
    love.graphics.rectangle( "fill", self.x, self.y, self.w, self.h, 8, 8, 5 )
    
    -- caption
    love.graphics.setFont(resources.fnt.unifontBold)
    love.graphics.setColor( 255, 255, 255, 255)
    love.graphics.printf(self.caption, self.x , self.y + self.margHCaption, self.w, "center")
    
    local scalef = (self.w - 2*self.margw)/self.pic:getWidth()
    
    -- image
    love.graphics.draw( self.pic, self.x + self.margw, self.y + self.margh, 0, scalef, scalef)
    
    local addMarginH = self.pic:getHeight()*scalef
    --text
    love.graphics.setFont(resources.fnt.unifont)
    love.graphics.setColor( 255, 255, 255, 200)
    love.graphics.printf(self.text, self.x + self.margw, self.y + self.margh + addMarginH, self.w - self.margw, "left")
  end
end

return(textpanel)