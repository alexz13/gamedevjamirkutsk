local mainmenu = {}
mainmenu.visible = false
-- Empty callbacks for menu
function mainmenu.onPlay()
  
end

function mainmenu.onLoad()
  
end

function mainmenu.onSave()
  
end

function mainmenu.onLevel()
  
end

function mainmenu.onExit()
  
end
  
function mainmenu:init()

end

function mainmenu:update(dt)
  
  interface.suit.layout:reset(300, 300 , 20,20)

    -- put a button at the layout origin
    -- the cell of the button has a size of 200 by 30 pixels
    if interface.suit.Button("PLAY", interface.suit.layout:row(200,30)).hit then
      mainmenu.onPlay()
    end
    
    if interface.suit.Button("LOAD", interface.suit.layout:row(200,30)).hit then
      mainmenu.onLoad()
    end
    
    if interface.suit.Button("SAVE", interface.suit.layout:row(200,30)).hit then
      mainmenu.onSave()
    end

    if interface.suit.Button("LEVEL", interface.suit.layout:row(200,30)).hit then
     mainmenu.onLevel()
    end
    
    interface.suit.layout:row()
    if interface.suit.Button("EXIT", interface.suit.layout:row(200,30)).hit then
      mainmenu.onExit()
    end

end

function mainmenu:draw()
  love.graphics.setColor( 255, 255, 255 )
	love.graphics.draw(resources.img.bgimage, 0, 0)
  interface.suit.draw()
  love.graphics.setColor( 166, 186, 230,180 )
  love.graphics.draw(resources.img.biglogo, (love.graphics.getWidth()- resources.img.biglogo:getWidth())/2, 100)
end

return(mainmenu)