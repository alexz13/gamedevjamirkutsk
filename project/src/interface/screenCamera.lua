local screenCamera = {
  cameraModule = require("libs.hump.camera")
  
  }

function screenCamera:init(xpos, ypos, zoom)
  screenCamera.cam = self.cameraModule.new(xpos, ypos, zoom, 0)
  
    -- величина  границы зоны отступа, при которой задействуется движение передвыижение камеры
  screenCamera.cameraScrollingBound = 50
  screenCamera:onResize()
                                 
  -- отображать зону в которой задействуется скролл камеры
  screenCamera.showcameraScrollingRanges = false
  
  -- отображать зону в которой задействуется скролл камеры
  screenCamera.enableCameraScrolling = true
  
  -- шаг скроллинга камеры
    screenCamera.cameraScrollingStep = 1
  
  -- период обработки скроллинга
  screenCamera.scrollPeriod = 0.005
  screenCamera.scrollPeriodTimer = 0
end


function screenCamera:setDimensions(weight, height)
    -- Задаём величину игрового поля в пикселах (по размеру фонового рисунка) 
  screenCamera.cameraRanges = utils.newArea(0, 0, weight, height)
end

function screenCamera:update(dt)
  screenCamera.scrollPeriodTimer = screenCamera.scrollPeriodTimer + dt
  if screenCamera.scrollPeriodTimer >= screenCamera.scrollPeriod then
    screenCamera.scrollPeriodTimer = 0
    if( self.enableCameraScrolling) then
      local dx = 0
      local dy = 0
      local cameraStep = screenCamera.cameraScrollingStep
      
      local xpos, ypos = love.mouse.getPosition()
      love.window.setTitle( xpos.." x "..ypos )
      --local xpos, ypos = 755,200
      
      if xpos <= screenCamera.cameraScrollingRanges.left then
        dx = -cameraStep
      elseif xpos >= screenCamera.cameraScrollingRanges.right then
        dx = cameraStep
      end
      if ypos <= screenCamera.cameraScrollingRanges.top then
        dy = -cameraStep
      elseif ypos >= screenCamera.cameraScrollingRanges.bottom and
             ypos < (screenCamera.cameraScrollingRanges.bottom + screenCamera.cameraScrollingBound) then
        dy = cameraStep
      end 
        
      if dx ~= 0 or dy ~= 0  then
          local width, height = love.graphics.getDimensions()
          local l,t = screenCamera.cam:worldCoords(0,0)
          local r,b = screenCamera.cam:worldCoords(width,height)
          
        if l <= screenCamera.cameraRanges.left and dx < 0 then dx = 0; end
        if t <= screenCamera.cameraRanges.top and dy < 0 then dy = 0; end
        if r >= screenCamera.cameraRanges.right and dx > 0 then dx = 0; end
        if b >= screenCamera.cameraRanges.bottom and dy > 0 then dy = 0; end
        
        screenCamera.cam:move(dx,dy)
        
      end  
    end
  end
end

function screenCamera:wheelmoved(x, y)
    if y > 0 then
        self.cam:zoom(1.25)
    elseif y < 0 then
        self.cam:zoom(0.8)
    end
end

function screenCamera:onResize()
  local width, height = love.graphics.getDimensions()
  -- обновляем зону в которой задействуется движение камеры
  self.cameraScrollingRanges = { left = self.cameraScrollingBound, 
                              top = self.cameraScrollingBound,
                              right = width - self.cameraScrollingBound,
                              bottom = height-interface.interfacePanel.panelh-self.cameraScrollingBound}
end

function screenCamera:draw()
  if self.showcameraScrollingRanges then
    love.graphics.setColor( 255, 255, 255 )
    love.graphics.rectangle('line',
      screenCamera.cameraScrollingRanges.left,
      screenCamera.cameraScrollingRanges.top,
      screenCamera.cameraScrollingRanges.right - screenCamera.cameraScrollingRanges.left,
      screenCamera.cameraScrollingRanges.bottom - screenCamera.cameraScrollingRanges.top)
  end
end

function screenCamera:getWorldMousePosition()
  return screenCamera.cam:worldCoords( love.mouse.getPosition() )
end

function screenCamera:attach()
  self.cam:attach()
end

function screenCamera:detach()
  self.cam:detach()
end

return screenCamera