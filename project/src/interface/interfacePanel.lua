local interfacePanel = { }

-- колбэк по умолчанию, который вызывается при наведении на кнопку
function interfacePanel.onPickDefault(idx, block) 
  if love.mouse.isDown(1) then
    interfacePanel.currentBlockType = block.tile
  end
end

-- конструктор кнопки
function interfacePanel:newblock(bname, btile, bcount, bonpick) 
  return {
      name = bname,
      tile = btile,
      count = bcount,
      highlighted = 0,
      choosen = false,
      onpick = self.onPickDefault;
      }
end

function interfacePanel:update(dt) 
  local mx, my = love.mouse.getPosition( )
  interfacePanel:checkIfBlocksPicked(mx, my)
end
  
    
function interfacePanel:init () 
  
  interfacePanel.visible = false;
  
  interfacePanel:onResize();

  -- ширина и высота смещения первой плитки на панели управления
  self.panelblockOffsw = 10
  self.panelblockOffsh = 10
  
  -- ширина и высота плитки на панели управления
  self.panelblockw = 80
  self.panelblockh = 80
  
  -- расстояние между плитками
  self.panelblockMarginw = 10
  
  -- смещение сверху изображения тайла на плитке 
  self.tileOffsH = 10
  
  self.blocks = { self:newblock('eraser', mech.BLOCK_EMPTY, -1),
                  self:newblock('power', mech.BLOCK_POWER, 5),
                  self:newblock('wire', mech.BLOCK_WIRE, 10),
                  self:newblock('moveup', mech.BLOCK_MOVEUP, 15),
                  self:newblock('moveleft', mech.BLOCK_MOVELEFT, 15),
                  self:newblock('moveright', mech.BLOCK_MOVERIGHT, 15),
                  self:newblock('asteriod', mech.BLOCK_ASTEROID, 15)--,
                 -- self:newblock('locator', BLOCK_LOCATOR, 15),
                 -- self:newblock('cont', BLOCK_CONTAINER, 15)
  }
  self.currentBlockType =  mech.BLOCK_EMPTY
end

function interfacePanel:onResize()
    local width, height = love.graphics.getDimensions()
  
  -- ширина и высота панели управления
  self.panelw = width
  self.panelh = 100

  -- координаты левого верхнего угла панели управления
  self.coordX = 0
  self.coordY = height - self.panelh
end

function interfacePanel:draw ()

  love.graphics.setColor( 0, 52, 79, 160 )
  love.graphics.rectangle( "fill", self.coordX, self.coordY, self.panelw, self.panelh )
  love.graphics.rectangle( "fill", self.coordX, self.coordY, self.panelw, self.panelh, 20, 20, 2 )
  
  for key, value in pairs(self.blocks) do
    self:drawButton(key, value)
  end
end

function interfacePanel:drawButton (i, block)
    
  local x = self.coordX + self.panelblockOffsw + (self.panelblockMarginw + self.panelblockw) * (i - 1)
  local y = self.coordY + self.panelblockOffsh 
  
  local alp = 100
  if block.highlighted == 1 then
    alp = 220
  end
  
  if(self.currentBlockType == block.tile)then
    alp = 255
  end
  
  love.graphics.setColor( 20, 62, 99, alp )
  love.graphics.rectangle("fill", x, y, self.panelblockw, self.panelblockh, 8,8,5 )
  love.graphics.setColor( 20, 62, 99, 220 )
  love.graphics.rectangle("line", x, y, self.panelblockw, self.panelblockh, 8,8,5 )
  

  --self.tileOffsH
  
  local tile = resources.img.tiles[block.tile]
  
  local x2 = x + self.panelblockw/2 - resources.img.tilew/2
  love.graphics.setColor( 255, 255, 255, 255 )
  love.graphics.draw(tile, x2 , y + self.tileOffsH)
  
  love.graphics.printf(block.name, x, y + self.tileOffsH * 1.5 + resources.img.tileh, self.panelblockw, "center")
  love.graphics.printf(block.count, x, y + self.tileOffsH * 3 + resources.img.tileh, self.panelblockw, "center")
end

-- 
function interfacePanel:checkIfBlocksPicked(mx, my)
  for key, value in pairs(self.blocks) do
      local x = self.coordX + self.panelblockOffsw + (self.panelblockMarginw + self.panelblockw) * (key - 1)
      local y = self.coordY + self.panelblockOffsh
      if mx > x and mx < (x + self.panelblockw) and my > y and my < (y + self.panelblockh) then
          self.blocks[key].highlighted = 1
          if(value.onpick ~= nil) then
            value.onpick(key,value)    
            return;
          end
      else
        self.blocks[key].highlighted = 0
      end
  end
end

return(interfacePanel)